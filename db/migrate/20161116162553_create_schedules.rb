class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :deptime
      t.string :aparttime
      t.string :city
      t.string :street

      t.timestamps null: false
    end
  end
end
