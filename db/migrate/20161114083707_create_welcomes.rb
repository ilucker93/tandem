class CreateWelcomes < ActiveRecord::Migration
  def change
    create_table :welcomes do |t|
      t.string :name
      t.string :phone
      t.date :date
      t.string :departure
      t.string :arrival
      t.integer :count

      t.timestamps null: false
    end
  end
end