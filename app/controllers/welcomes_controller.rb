class WelcomesController < ApplicationController 
  def index
    @welcomes = Welcome.all
    @welcome = Welcome.new
  end

  def new
    @welcome = Welcome.new
  end

  def create
    @welcome = Welcome.new(welcome_params)
      if @welcome.save
        WelcomeMailer.welcome_email(@welcome).deliver_now  # welcomemailer общий класс, welcome_email - это класс, и переменная
        redirect_to root_path
      else 
        render 'new'
      end
  end

  def show 
    @welcome = Welcome.find(params[:id])
  end

  def robots
    respond_to :text
    expires_in 6.hours, public: true
  end

private
  def welcome_params
    params.require(:welcome).permit(:name, :phone, :departure, :arrival, :date, :count)
  end
end
