class WelcomeMailer < ApplicationMailer
	default from: "tandemtour11@yandex.ru"
	def welcome_email(welcome)
		@welcome = welcome
	    mail(to: 'tandemtour11@yandex.ru' , subject: 'Бронь')
	end
end
